@extends('layouts.index')
@section('content')
<main>
    <div class="container-fluid px-4 title-category">
        <h3 class="mt-4">List Categories</h3>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item">Category</li>
            <li class="breadcrumb-item active">Next</li>
        </ol>
        <div class="container">
            <div class="row">
                <div class="col-12" style="text-align: right;">
                    <a href="{{route('categories.create')}}" class="btn btn-primary" role="button">Add Category</a>
                </div>
            </div>
        </div>
        @if (count($categories) > 0)
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="col-12">
                        @if(Session::has('cat_update'))
                        <div class="alert alert-success"><em>{!! session('cat_update') !!}</em></div>
                        @endif
                    </div>
                </div>
                <div class="col-12">
                    <table class="table table-bordered mt-2">
                        <thead>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach ($categories as $category)
                            <tr>
                                <td>
                                    <div>{{$category->id}}</div>
                                </td>
                                <td>
                                    <div>{{$category->name}}</div>
            
                                </td>
                                <td>
                                    <div>{!! $category->description !!}</div>
                                </td>
            
                                <td>
            
                                    <a class="btn btn-primary btn-sm" href="{{route('categories.edit',$category->id) }}">Edit</a>
                                    <!--delete category -->
                                    <form action="{{route('categories.destroy', $category->id)}}" method="post" style="display: inline-block">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @endif
</main>
@endsection