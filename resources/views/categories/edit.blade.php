@extends('layouts.index')
@section('content')
<main>
    <div class="container-fluid title-category">
        <h3 class="mt-4">Update Category</h3>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{url('categories')}}">Categories</a></li>
            <li class="breadcrumb-item active">Next</li>
        </ol>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    @if(Session::has('cat_update'))
                    <div class="alert alert-success"><em>{!! session('cat_update') !!}</em></div>
                    @endif
                </div>
                <div class="col-12">
                    <form method="post" action="{{url('categories',$categories->id)}}">
                        <div class="card">
                            <div class="card-header text-center">
                                <h3>Update Category</h3>
                            </div>
                            <div class="card-body">
                                @csrf
                                @method('PATCH')
                                <div class="form-group">
                                    <label for="name">Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name"  value="{{ $categories->name }}">
                                    @error('name')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" style="height:150px" name="description" placeholder="description">{{ $categories->description }}</textarea>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary mt-2">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>

@endsection