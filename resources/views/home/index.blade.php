@extends('layouts.index')
@section('content')
<main>
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="title">
                        <h1>Welcome To Laravel Developer</h1>
                    </div>
                    <div class="text">
                        <p>
                            Laravel is a web application framework with expressive, elegant syntax. A web
                             framework provides a structure and starting point for creating your applica
                             tion, allowing you to focus on creating something amazing while we sweat the
                              details. Laravel strives to provide an amazing developer experience while p
                              roviding powerful features such as thorough dependency injection, an express
                              ve database abstraction layer, queues and scheduled jobs, unit and integrati
                              on testing, and more. Whether you are new to PHP or web frameworks or have ye
                              ars of experience, Laravel is a framework that can grow with you. We'll help 
                              you take your first steps as a web developer or give you a boost as you take 
                              your expertise to the next level. We can't wait to see what you build.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection