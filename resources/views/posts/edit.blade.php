@extends('layouts.index')
@section('content')
<main>
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3 class="mt-4">List Posts</h3>
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item">Posts</li>
                        <li class="breadcrumb-item active">Next</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form method="post" action="{{route('posts.update',$post->id)}}" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <div class="card">
                            <div class="card-header text-center">
                                <h3>Update Post</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="title">Title</label>
                                            <input type="text" class="form-control" id="title" name="title" value="{{ $post->title }}">
                                        </div>
                                    </div>
                                    <div class="col-6 mt-2">
                                        <div class="form-group">
                                            <label for="author">Author</label>
                                            <input type="text" class="form-control" id="author" name="author" value="{{ $post->author }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 mt-2">
                                        <div class="form-group">
                                            <label for="cat_id">Category</label>
                                            <input type="text" class="form-control" id="cat_id" name="category_id" value="{{ $post->category_id }}">
                                        </div>
                                    </div>
                                    <div class="col-6 mt-2">
                                        <div class="form-group">
                                            <label for="image">Image</label>
                                            <input type="text" class="form-control" id="image" name="image" value="{{ $post->image }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 mt-2">
                                        <div class="form-group">
                                            <label for="short_desc">Short_Desc</label>
                                            <textarea class="form-control" style="height:150px" name="short_desc" placeholder="Short_Desc">{{ $post->short_desc }}"</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 mt-2">
                                        <div class="form-group">
                                            <label>Description:</label>
                                            <textarea class="form-control" style="height:150px" name="description" placeholder="Description">{{ $post->description }}"</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary mt-2">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>

{{-- <div class="mt-2">
    @include('common.errors')
    @if(Session::has('category_create'))
    <div class="alert alert-success"><em>{!! session('category_create') !!}</em>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times</span></button>
    </div>
    @endif
</div> --}}
@endsection