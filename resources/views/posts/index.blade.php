@extends('layouts.index')
@section('content')
<main>
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3 class="mt-4">List Posts</h3>
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item">Posts</li>
                        <li class="breadcrumb-item active">Next</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a href="posts/create" class="btn btn-primary" role="button">New Posts</a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    @if (count($posts) > 0)
                        <div class="card-deck mb-2 mt-2">
                            @foreach ($posts as $post)
                            <div class="card mt-2">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-2">
                                            <div class="flex-shrink-0">
                                                <img class="" src="{{asset('post/assets/img/'.$post->image)}}" width="100px" class="rounded-circle" alt="..." />
                                            </div>
                                            <h6 class="card-title">Author : {{$post->author}}</h6>
                                        </div>
                                        <div class="col-10">
                                            <h5 class="card-title">Title : {{$post->title}}</h5>
                                            <p class="card-text"><strong>Short_Desc</strong> : {{$post->short_desc}}</p>
                                            <p class="card-text"><strong>Description</strong> : {{$post->description}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer" style="text-align: right;">
                                    <a class="btn btn-primary btn-sm" href="{{route('posts.edit',$post->id)}}">Edit</a>
                                    <!--delete post -->
                                    <form action="{{route('posts.destroy', $post->id)}}" method="post" style="display: inline-block">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                                    </form>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</main>
@endsection