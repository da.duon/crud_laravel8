@extends('layouts.index')
@section('content')
<main>
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3 class="mt-4">Create Post</h3>
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item"><a href="{{url('posts')}}">Posts</a></li>
                        <li class="breadcrumb-item active">Next</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form method="post" action="{{route('posts.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="card">
                            <div class="card-header text-center">
                                <h4 class="text-center">Create Posts</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6 mt-2">
                                        <div class="form-group">
                                            <label for="title">Title<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="title" name="title" placeholder="title" value="{{old('title')}}">
                                        </div>
                                    </div>
                                    <div class="col-6 mt-2">
                                        <div class="form-group">
                                            <label for="author">Author<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="author" name="author" placeholder="author" value="{{old('author')}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6 mt-2">
                                        <div class="form-group">
                                            <label for="title">Select Category<span class="text-danger">*</span></label>
                                            <select name="category_id" id="" class="form-control">
                                             @foreach($categories as $cat)
                                             <option value="{{$cat->id}}">{{$cat->name}}</option>
                                             @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6 mt-2">
                                        <div class="form-group">
                                            <label for="image">Photo</label>
                                            <input type="file" class="form-control" id="image" name="image">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 mt-2">
                                    <div class="form-group">
                                        <label for="short_desc">Short Description</label>
                                        <textarea class="form-control" name="short_desc" placeholder="short description">{{old('short_desc')}}</textarea>
                                    </div>
                                </div>
                                <div class="col-12 mt-2">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea class="form-control" name="description" placeholder="sescription">{{old('description')}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
{{-- <div class="mt-2">
    @include('common.errors')
    @if(Session::has('category_create'))
    <div class="alert alert-success"><em>{!! session('category_create') !!}</em>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times</span></button>
    </div>
    @endif
</div> --}}
@endsection