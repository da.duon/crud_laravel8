<?php

namespace App\Http\Controllers;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Post;

class PostController extends Controller
{

    public function index()
    {
        $posts = Post::all();
        return view('posts.index')->with('posts', $posts);
    }
    public function create()
    {
        $categories = Category::all();
        return view('posts.create')->with('categories', $categories);
    }

    public function store(Request $request)
    {
        $updateData = $request->validate([
            'category_id' => 'required',
            'title' => 'required',
            'image' => 'required',
            'author' => 'required',
            'short_desc' => 'required',
            'description' => 'required',
        ]);

        // Create The Post
        $post = new Post;
        $image = $request->file('image');
        $filename = time() . '.' . $image->getClientOriginalExtension();
       
        $upload = 'post/assets/img/';
        $filename = time().$image->getClientOriginalName();
        $path = move_uploaded_file($image->getPathName(), $upload. $filename);

        if($request->file('image') != ""){
            $image = $request->file('image');
            $upload = 'post/assets/img/';
            $filename = time().$image->getClientOriginalName();
            $path = move_uploaded_file($image->getPathName(), $upload. $filename);
		}

        $post->category_id = $request->category_id;
        $post->title = $request->title;
        $post->image = $filename;
        $post->author = $request->author;
        $post->short_desc = $request->short_desc;
        $post->description = $request->description;
        $post->save();
        return redirect()->back();
   }
   public function destroy($id)
   {
       $posts = Post::findOrFail($id);
       $posts->delete();

       return redirect('/posts')->with('completed', 'Posts has been deleted');
   }

   public function edit($id)
    {
        $post = Post::find($id);
        return view('posts.edit')->with('post', $post);
    }

    public function update(Request $request, $id)
    {
        $updateData = $request->validate([
            'category_id' => 'required|integer',
            'title' => 'required',
            'image' => 'required',
            'author' => 'required',
            'short_desc' => 'required',
            'description' => 'required',
        ]);
        Post::whereId($id)->update($updateData);
        return redirect('/posts')->with('completed', 'Catgory has been updated');
    }
}
