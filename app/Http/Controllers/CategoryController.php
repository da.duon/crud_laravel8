<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Session;
class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('categories.index')->with('categories', $categories);
    }
    public function create()
    {
        return view('categories.create');
    }
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
        ]);
        Category::create([
            'name' => $request->name,
            'description' => $request->description
        ]);
        Session::flash('cat_create', 'Create successfull!');
        return redirect()->back();
    }


    public function edit($id)
    {
        $categories = Category::find($id);
        return view('categories.edit')->with('categories', $categories);
    }

    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'name' => 'required',
        ]);
        Category::find($id)->update([
            'name' => $request->name,
            'description' => $request->description
        ]);
        Session::flash('cat_update', 'Updated successfull!');
        return redirect()->back();
    }

    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();
        Session::flash('cat_detele', 'Deleted successfull!');
        return redirect()->back();
    }

}
