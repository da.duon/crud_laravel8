<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CategoryController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get("/category","App\Http\Controllers\CategoryController@index");
// Route::post("/category/create","App\Http\Controllers\CategoryController@create");
// Route::post("/category","App\Http\Controllers\CategoryController@store");
// Route::get("/category/{categoryId}/edit","App\Http\Controllers\CategoryController@edit");
// Route::put("/category/{categoryId}","App\Http\Controllers\CategoryController@update")->name('category.update');
// Route::delete("/category/{categoryId}","App\Http\Controllers\CategoryController@destroy");
// Route::get('/admin', function () {
//     return view('admin.index');
// });

Route::get('/', function () {
    return view('home.index');
});


Route::resource('posts', PostController::class);
Route::resource('categories', CategoryController::class);